# Mi Sistema Operativo

![mi escritorio](captura/escritorio.png)

> Mi versión de GNU/Linux con los programas que me instalé y sus configuraciones.

## ¿Que busco?

Tener un **sistema operativo** pensado para editar imágenes, video y animación, textos y compartir datos.
Pensado para ser "liviano", auto configurado, descentralizar los datos y no necesitar tener que usar internet todo el tiempo.

## Programas

### Escritorio

Busco un escritorio con sombras y con una barra inferior que se oculta rapidamente. No uso menu, al estilo windos.
El menú lo reemplace con lxlauncher, este programa ocupa el escritorio y lo puedo ver moviendo el mouse a la esquiza inferior izquierda de la pantalla.

* lxde		→	escrito
* compton	→	sombras y transparencias
* brightside	→	atajos con el mouse y el escritorio
* skippy-xd	→	para elegir ventanas
* lxlauncher	→	reemplazar el menu
* parcellite	→	portapapeles con historial
* alarm-clock-applet	→	alarma
* xpad	→ para notas en el escritorio
* cryptkeeper	→	carpeta cifrada muy simple de usar
* gigolo	→	conectarme a otras maquinas en la red

#### Miniaturas

* [video]()	uso esto para no necesitar totem
* [scad](https://github.com/maquinas-libres/openscad-thumbnailer)
* [stl](https://github.com/maquinas-libres/openscad-thumbnailer)
* [xcf](https://github.com/4232/xcf-thumbnailer)
* [sifz](https://github.com/4232/synfig-thumbnailer)

### Arte

Dibujo, pintura, reproducir vídeos, etc

* inkscape	→	editor de **imágenes vectoreales**
* gimp		→	editor de **imágenes pixelares**
* imagemagick	→	editor de imágenes desde la terminal
* synfigstudio	→	editor de **animación**
* kdenlive	→	editor de **videos**
* geeqie	→	organizador imagenes con etiquetas
* mpv	→	reproductor de vídeo 
 * [autosub](https://github.com/vayan/autosub-mpv)	→	descarga subtitulos
 * [delogo](https://github.com/b4zz4/mpv-delogo)	→	quita el logo de un video
 * [deframe](https://github.com/b4zz4/mpv-deframe))	→	quita los marcos con los que suben algunos videos
* simple scan		→	programa para escáner de modo simple
* [inklingreader](https://github.com/roelj/inklingreader)	→	software para mi tableta gráfica

### Editores de textos

* gedit			→	editor de textos
* pandoc		→	convertir textos a diversos formatos
* pdf2html		→	convertir pdf a html
* aspell-es		→	corrector de ortografía
* apertium		→	traductor (no es muy bueno, pero ayuda)
 * apertium-en-es	→	ingles español
 * apertium-fr-es	→	frances español

### Compartir

* mldonkey-server	→	p2p con [cliente web](http://127.0.0.1:4080)
* transmission			bittorrent
* mini-httpd		→	para servidor web

### Navegador

Navegar en internet sin publicidad, mejorando las búsquedas, [no ser monitoreado](http://prism-break.org/) y no necesitando tanto de las nubes centralizadas.
[Add-ons](https://www.gnu.org/software/gnuzilla/addons.html) libre para icecast, firefox, iceweasel.

* firefox		→	navegador web
 * brief		→	Lector de noticias y novedades (por defecto con tor)
* youtube-dl			descargar videos
 * [someonewhocares](http://someonewhocares.org/hosts/)	→	bloquear publicada en base a `/etc/hosts`

### Redes

* tor
* torsocks
* proxychains	→	Un modo simple de usar el Tor y/u otro proxy

### Romper redes

Es el modo mas simple y fácil que encontré para romper redes.

* kali

#### Comunicación

* pidgin	→	mensajería
 * pidgin-otr	→	otr para pidgin
 * pidgin-torchat	→	mensajería p2p
 * bounjour	→	mensajería en la [red local](https://es.wikipedia.org/wiki/Red_de_%C3%A1rea_local)
* thunderbrid	→	para ser parte de la red social mas grande del planeta (el mail)
 * enigma	→	comunicación segura

### Otros

* localepurge	→	borra los archivos de idioma que no usamos
* xsel 		→	copiar al portapapeles desde la terminal

## Que trato de hacer

- Que funcione en varios Sistemas Operativos
- Que sea muy rapido liviano y lindo
- Destinado a gráficos y animación

-- 

**PayPal:** [un $1 para la birra](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=JMFARRBCYTFG8)    
**Bitcoin:** 19qkh5dNVxL58o5hh6hLsK64PwEtEXVHXs
